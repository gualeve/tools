#!/usr/bin/python
'''
    FILENAME :   du.py

    DESCRIPTION :
        List disk space of directory in Linux, using command 'du'.

    USAGE :
        $ python du.py directory

    AUTHOR      :   José Adalberto F. Gualeve       STARTDATE: 12-07-2018
                                                    UPDATE: 10-29-2021

'''
from os import popen
from sys import argv

def kmg(tamanho):
    unidades = ['K', 'M', 'G']
    i = 0
    while ( tamanho > 1024):
        tamanho = tamanho / 1024
        i += 1
    tamanho = round(tamanho, 1)
    return str(tamanho) + unidades[i]

def du(dir):
    ls = popen('ls -1 {}'.format(dir)).read().splitlines()
    print('        SIZE  FILENAME')
    data_dir = []
    for file in ls:
        data_file = popen('du -s "{}/{}"'.format(dir, file)).read().split()
        data_file[0] = int(data_file[0])
        data_file.append(kmg(data_file[0]))
        data_dir.append(data_file)
    data_dir.sort()
    for file in data_dir:
        print('{:12}  {}'.format(file[2], file[1]))


if __name__ == '__main__':
    if len(argv) < 2:
        print('Usage:')
        print('$ python du.py <DIR>')
    else:
        du(argv[1])
